class GameItem {
}
/
    * Function;
to;
create;
the;
GameItem
    * ;
{
    string;
}
-name
    * ;
{
    number;
}
-xPosition
    * ;
{
    number;
}
-yPosition
    * /;
constructor(name, string, xPosition, number = 0, yPosition, number = 0);
{
    this._name = name;
    this._xPos = xPosition;
    this._yPos = yPosition;
}
set;
xPos(xPosition, number);
{
    this._xPos = xPosition;
}
set;
yPos(yPosition, number);
{
    this._yPos = yPosition;
}
/
    * Function;
to;
draw;
the;
initial;
state;
of;
the;
gameItem
    * ;
{
    HTMLElement;
}
-container
    * /;
draw(container, HTMLElement);
void {
    this: ._element = document.createElement('div'),
    this: ._element.className = this._name,
    this: ._element.id = this._name,
    this: ._element.style.transform = translate($, { this: ._xPos }, px, $, { this: ._yPos }, px),
    const: image = document.createElement('img'),
    image: .src = . / assets / $
};
{
    this._name;
}
png;
this._element.appendChild(image);
container.appendChild(this._element);
update();
void {
    this: ._element.style.transform = translate($, { this: ._xPos }, px, $, { this: ._yPos }, px)
};
class car extends GameItem {
}
/
    * Function;
to;
create;
the;
Character
    * ;
{
    string;
}
-name
    * ;
{
    number;
}
-xPosition
    * ;
{
    number;
}
-yPosition
    * /;
constructor(name, string, xPosition, number = 0, yPosition, number = 0);
{
    super(name, xPosition, yPosition);
}
/
    * Function;
to;
move;
the;
Character;
upwards
    * ;
{
    number;
}
-yPosition
    * /;
move(xPosition, number);
void {
    this: ._xPos -= xPosition,
    this: ._element.classList.add('driving')
};
class parkingspot extends GameItem {
}
/
    * Function;
to;
create;
the;
GameItem
    * ;
{
    string;
}
-name
    * ;
{
    number;
}
-id
    * ;
{
    number;
}
-xPosition
    * ;
{
    number;
}
-yPosition
    * /;
constructor(name, string, id, number, xPosition, number = 0, yPosition, number = 0);
{
    super(name, xPosition, yPosition);
    this._id = id;
}
/
    * Function;
to;
draw;
the;
initial;
state;
of;
the;
coin
    * ;
{
    HTMLElement;
}
-container
    * /;
draw(container, HTMLElement);
void {
    this: ._element = document.createElement('div'),
    this: ._element.className = this._name,
    this: ._element.id = $
};
{
    this._name;
}
-$;
{
    this._id;
}
;
this._element.style.transform = translate($, { this: ._xPos }, px, $, { this: ._yPos }, px);
const image = document.createElement('img');
image.src = . / assets / $;
{
    this._name;
}
png;
this._element.appendChild(image);
container.appendChild(this._element);
remove(container, HTMLElement);
void {
    const: elem = document.getElementById($, { this: ._name } - $, { this: ._id }),
    container: .removeChild(elem)
};
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class parkingspot extends GameItem {
}
/
    * Function;
to;
create;
the;
GameItem
    * ;
{
    string;
}
-name
    * ;
{
    number;
}
-id
    * ;
{
    number;
}
-xPosition
    * ;
{
    number;
}
-yPosition
    * /;
constructor(name, string, id, number, xPosition, number = 0, yPosition, number = 0);
{
    super(name, xPosition, yPosition);
    this._id = id;
}
/
    * Function;
to;
draw;
the;
initial;
state;
of;
the;
coin
    * ;
{
    HTMLElement;
}
-container
    * /;
draw(container, HTMLElement);
void {
    this: ._element = document.createElement('div'),
    this: ._element.className = this._name,
    this: ._element.id = $
};
{
    this._name;
}
-$;
{
    this._id;
}
;
this._element.style.transform = translate($, { this: ._xPos }, px, $, { this: ._yPos }, px);
const image = document.createElement('img');
image.src = . / assets / $;
{
    this._name;
}
png;
this._element.appendChild(image);
container.appendChild(this._element);
remove(container, HTMLElement);
void {
    const: elem = document.getElementById($, { this: ._name } - $, { this: ._id }),
    container: .removeChild(elem)
};
class Scoreboard extends GameItem {
}
/
    * Function;
to;
create;
the;
GameItem
    * ;
{
    string;
}
-name
    * ;
{
    number;
}
-xPosition
    * ;
{
    number;
}
-yPosition
    * /;
constructor(name, string);
{
    super(name);
    this._score = 0;
}
get;
score();
number;
{
    return this._score;
}
/
    * Function;
to;
draw;
the;
initial;
state;
of;
the;
gameItem
    * ;
{
    HTMLElement;
}
-container
    * /;
draw(container, HTMLElement);
void {
    this: ._element = document.createElement('div'),
    this: ._element.className = this._name,
    this: ._element.id = this._name,
    const: p = document.createElement('p'),
    p: .innerHTML = 'The score is: ',
    const: span = document.createElement('span'),
    span: .innerHTML = this._score.toString(),
    p: .appendChild(span),
    this: ._element.appendChild(p),
    container: .appendChild(this._element)
}
    /
    * Function;
to;
update;
the;
state;
of;
the;
Scoreboard in the;
DOM
    * / ;
update();
void {
    const: scoreSpan = this._element.childNodes[0].childNodes[1],
    scoreSpan: .innerHTML = this._score.toString()
}
    /
    * Function;
to;
add;
the;
score;
with (1
    * / )
    public;
addScore();
void {
    this: ._score += 1
};
//# sourceMappingURL=main.js.map